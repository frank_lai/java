**一、Spring 事务管理**


1.基於註解

a.配置事務管理器以及開啟事務註解

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321224136685-1520614557.png)

 

tx:annotation-driven中 transaction-manager的默認值是transactionManager

如果當前事務管理器的id值就是transactionManager，那麼transaction-manager的配置

可以省略，如果當前事務管理器的ID值不是transactionManager，那麼就必須在transaction-manager

中指定當前的事務管理器的id值


b.@Transactional

該注解可以加到類上，也可以加到方法上。加到類上的對當前類中所有的方法都起作用。

如果類與方法都加了該註解，則方法上的優先於類上的



c.事务的属性
![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321224209535-858590659.png)

 


2.基於xml

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321224220182-205574961.png)

 