**一、基於注解的方式配置bean/bean的裝配**


1.常用的注解


@Component:標示為一個组件
@Repository:標示為一個持久层的组件
@Service:標示為一個業務層的组件
@Controller:標示為一個表现层的组件
@AutoWired:自動裝配
@Qualifier:具體的指定要裝配的bean的id值(多型的時候會用到)


2.開啟注解配置:


<context:component-scan base-package="PACKAGE包"></context:component-scan>
use-default-filters="true/false":是否使用默認的過濾
<context:include-filter type="annotation/assignable"expression="注解的全類名/組件類的全類名/>
<context:include-filter>需要配合 use-default-fiters="false"來使用
<context:exclude-filter type="annotation/assignable"expression="注解的全類名/組件類的全類名"/>

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223056799-854607614.png)

 

 

3.@AutoWired


a.優先匹配兼容的類型來進行注入.如果有多个兼容的類型匹配,則再嘗試使用被注入的属性名，跟ioc容器中bean的id值进行匹配。


b.required: 默認情况下,被 @AutoWired 標註的属性必须要被裝配.可以使用required="false"來改變默認的行為.
![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223126143-551189417.png)

 

c.可以使用@Qualifier來具體指定要裝配的bean的id值.

 ![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223132327-218282656.png)