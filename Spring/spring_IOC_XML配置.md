**一、Spring之HelloWorld**

**1.導入所需的jar**

**a.一般方法**

commons-logging-1.1.1.jar
spring-beans-4.0.0.RELEASE.jar
spring-context-4.0.0.RELEASE.jar
spring-core-4.0.0.RELEASE.jar
spring-expression-4.0.0.RELEASE.jar


**b.Maven**

```xml
<properties>

<spring.version>4.0.0.RELEASE</spring.version><commons.logging.version>1.1.1</commons.logging.version><junit.version>4.12</junit.version>

</properties>

<dependencies>

<dependency>

<groupId>org.springframework</groupId>
<artifactId>spring-core</artifactId>
<version>${spring.version}</version>

</dependency>
<dependency>

<groupId>org.springframework</groupId>
<artifactId>spring-beans</artifactId>
<version>${spring.version}</version>

</dependency>
<dependency>

<groupId>org.springframework</groupId>
<artifactId>spring-context</artifactId>
<version>${spring.version}</version>

</dependency>
<dependency>

<groupId>org.springframework</groupId>
<artifactId>spring-context</artifactId>
<version>${spring.version}</version>

</dependency>
<dependency>

<groupId>commons-logging</groupId>
<artifactId>commons-logging</artifactId>
<version>${commons.logging.version}</version>

</dependency>

<!-- 配置junit --><dependency>

<groupId>junit</groupId><artifactId>junit</artifactId><version>${junit.version}</version><scope>test</scope>

</dependency>

</dependencies>
```

 


2.創建applicationContext.xml配置文件

a.創建Spring Bean Configuration File

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321215907608-1062001210.png)

 

b.創建IOC容器，並容器中獲取bean

 

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220008173-85123186.png)

 

**二、IOC容器**

 

1.IOC(Inversion of Control):其思想是**反轉資源獲取的方向**. 傳統的資源查找方式要求組件向容器發起請求查找資源. 做為 回應容器適時的返回資源. 而應用了IOC容器之後, **則是容器主動的將資源推送給他所管理的組件, 組件所要做得僅是選擇一種合適的方式來接受資源**.這種行為也被稱為查找的被動形式


2.BeanFactory:面向框架本身的.是IOC容器的基礎設施.


3..ApplicationContext:面向開發者.


ClasspathXmlApplicationContext.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220213971-405825190.png)

 

 

**三、依賴注入**


1.DI(Dependency Injection) — IOC 的另一種表達方式：**即組件已一些預先定義好的方式(例如:setter方法)接受來自如容器的資源注入**. 相對於IOC而言，這種表達方式更直接.


2.set方式注入:需提供成員變量對應的SET方法.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220540022-2052060956.png)

 

3.建構式注入:提供對應的建構器.

 ![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220552518-1237505427.png)

 


4.注入的细節

a.如果遇到特殊字符的注入:

\>1.使用轉移字符/實體，可以參可手冊
\>2.使用<![CDATA[...]]>的方式****
![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220623981-498761969.png)

 



b.可以使用value/ref属性的方式注入，也可以使用<value>/<ref>子標籤的方式注入

 

4.List/Map類型的注入


a.List/Set/Array:Spring提供了對應的<list> <set> <array>標籤來注入對應的類型的值.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220652661-1185115668.png)

 


b.Map:Spring提供了<map>以及<entry>子標籤來進行map類型值的注入.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220700645-506350914.png)

 

5.内部bean:定義到一個bean的内部的bean。内部bean只能在内部使用.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220710252-1231551765.png)

 


**四、p命名空間**


a.首先使用前需要導入p命名空间

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220746407-1691498220.png)

 

b.在<bean>標籤中使用 p:属性名/ p:属性名-ref 的方式進行值的注入

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220753150-847860962.png)

 


**五、自動裝配**


a.自動裝配引用類型的屬性.


b.使用autowire属性來指定裝配的方式


c.自動裝配的方式:


**byName**:通過要裝配的属性的名字與 IOC容器中bean的id值進行匹配

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220828151-420111384.png)

 

**byType**:通過要裝配的属性的類型與 IOC容器中bean的class值進行匹配,如果匹配到多個兼容的類型，會有異常.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220838447-1628183880.png)

 

**六、bean之間的關係**


a.繼承:繼承後可以複寫父bean的屬性

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220914120-254348352.png)

 

b.依賴: 依賴就是指定關係，並不會賦值。

 ![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321220918261-1703069113.png)

 

**七、bean的作用域**


a.singleton: 單例(默認值),在IOC容器中，只有一個該bean的實例物件.並且該bean的物件，**會**在**IOC容器初始化時創建**


b.prototype: 原型，在IOC容器中，有多個该bean的實例物件.**不會**在**IOC容器初始化**的時候**創建**,而是在**每次getBean**的時候，才會**創建一個新的物件**返回


c.request:一次請求期間


d.session:一次會話期間.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221227678-23990436.png)

 


**八、引入外部化的配置文件**

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221251954-1588481519.png)

 


db.properties:

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221259314-1199730929.png)

 


**九、bean的生命周期**


a.調用建構式創建物件


b.給物件的屬性設置值


c.調用init方法進行初始化


d.使用物件


e.調用destroy方法進行物件的銷毀.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221413657-192095551.png)

 


**十、bean的配置方式**


a.全類名(反射)的方式

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221421474-2051229848.png)

 


b.工廠方法


\>.靜態工廠方法

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221428535-1968257439.png)

 

\>.實例工廠方法

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221435354-1524191407.png)

 

c.FactoryBean

  *![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321221447554-271597505.png)*