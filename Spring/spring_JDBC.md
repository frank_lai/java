一、Spring Jdbc


1.jdbcTemplate

Spring將原始的jdbcAPI重新封裝提供了jdbcTemplate的模板類.

2.配置JAR


a.maven

```xml
<!-- 配置spring JDBC -->
<dependency>

<groupId>org.springframework</groupId>
<artifactId>spring-jdbc</artifactId>
<version>${spring.version}</version>

</dependency>
```

b.手動配置:**spring-jdbc-4.3.3.RELEASE.jar**


2.常用方法:


update: 執行增刪改

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223859877-1404850633.png)

 


batchUpdate:執行批量增刪改

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223915178-426687348.png)

 


QueryForObject:查詢單行返回單個物件/查詢返回單個值

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223941059-1564442108.png)

 

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321223954764-1120202480.png)

 


query:查詢多行返回多個物件

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321224001158-937927601.png)

 

3.在Dao中使用jdbcTemplate:


在Dao類中定義jdbcTemplate，然後通過@AutoWired将jdbcTemplate注入.

![img](https://img2018.cnblogs.com/blog/1639204/201903/1639204-20190321224027951-1881324977.png)

 