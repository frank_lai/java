# Summary

# 程式語言:

# 	[JAVA](README.md)



# JAVA框架:

# 	[Spring](spring.md)

​	1.[spring_IOC_XML配置](https://gitlab.com/frank_lai/java/blob/master/Spring/spring_IOC_XML配置.md) 

​	2. [spring_IOC_自動裝配](https://gitlab.com/frank_lai/java/blob/master/Spring/spring_IOC_自動裝配.md) 

​	3. [spring_AOP](https://gitlab.com/frank_lai/java/blob/master/Spring/spring_AOP.md)

​	4. [spring_JDBC](https://gitlab.com/frank_lai/java/blob/master/Spring/spring_JDBC.md) 

​	5. [spring_事務](https://gitlab.com/frank_lai/java/blob/master/Spring/spring_事務.md)

# 	[Mybatis](mybatis.md)

# 	[SpringMVC](springMVC.md)

# 	[SpringBatch](springbatch.md)

# 	[JPA](jpa.md)	

​		

# 資料庫

# [	REDIS](redis.md)

